## Об алгоритме
Так как по условию необходимо сгенерировать промо-коды вида XX-XXXX1234 без возможности использования ```Math.random()``` для первых шести символов, то логично делать это с помощью разбиения даты вызова функции для генерирования промо-кодов и таблицы ASCII.

## X1, X2, X3, X4
Первые четыре символа генерируются похожим образом:

```js
let date = new Date(),
    x1 = date.getUTCDay() + 75,
    x2 = date.getUTCHours() + 65,
    x3 = date.getUTCMonth() + 70,
    x4 = date.getUTCFullYear() - 2017 + 65,
```

Я прибавил к каждому символу минимум 65, чтобы получить значение, которое лежит в пределе от 65 до 90, поскольку в этом диапозоне располагаются буквы латинского алфавита в таблице ASCII.

## X5, X6
С X5 и X6 аналогичный метод не сработает, поэтому я поступил так:

```js
let trash1 = date.getUTCMinutes().toString(), //минуты
    trash2 = date.getUTCSeconds().toString(); //секунды

trash1.length > 1 ? x5 = parseInt(trash1[1]) - parseInt(trash1[0]) + 70 : x5 = parseInt(trash1[0]) + 80;
trash2.length > 1 ? x6 = parseInt(trash2[1]) - parseInt(trash2[0]) + 70 : x6 = parseInt(trash2[0]) + 80;
```

## X7, X8
X7 - это количество миллисекунд. Если оно трёхначное, то в конец дописываем один ноль, если двухзначное - два нуля и т.д. Общий код выглдяит так:

```js
function generateCode() {
    let date = new Date(),
        x1 = date.getUTCDay() + 75, //день
        x2 = date.getUTCHours() + 65, //часы
        x3 = date.getUTCMonth() + 70, //месяц
        x4 = date.getUTCFullYear() - 2017 + 65, //год
        x5, x6;

    let trash1 = date.getUTCMinutes().toString(), //минуты
        trash2 = date.getUTCSeconds().toString(), //секунды
        x7 = date.getUTCMilliseconds(); //миллисекунды

    trash1.length > 1 ? x5 = parseInt(trash1[1]) - parseInt(trash1[0]) + 70 : x5 = parseInt(trash1[0]) + 80;
    trash2.length > 1 ? x6 = parseInt(trash2[1]) - parseInt(trash2[0]) + 70 : x6 = parseInt(trash2[0]) + 80;

    x7 >= 0 && x7 < 10 ? x8 = '000' : // последнее число трёхзначное
    x7 > 9 && x7 < 100 ? x8 = '00' : //последнее число двухзначное
    x8 = '0';

    return `${String.fromCharCode(x1, x2)}-${String.fromCharCode(x3, x4, x5, x6)}${x7}`+x8;
};
```


